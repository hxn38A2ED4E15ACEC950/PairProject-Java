package work01;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class charCount
 */
@WebServlet("/charCount")
public class charCount extends HttpServlet {
       
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public charCount() {
        super();
        // TODO Auto-generated constructor stub
    }


	 //按值的大小进行降序，字典排序
    public static void sortMap(Map<String,Integer> oldmap,HttpServletRequest req, HttpServletResponse resp) throws IOException{
        ArrayList<Map.Entry<String,Integer>> list = new ArrayList<Map.Entry<String,Integer>>(oldmap.entrySet());
        Collections.sort(list,new Comparator<Map.Entry<String,Integer>>(){
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                if (o1.getValue() == o2.getValue()) {
                    return o1.getKey().compareTo(o2.getKey());//词频相同时，按字典排列
                }
                return o2.getValue() - o1.getValue();  //降序排列
            }
        });
        resp.setContentType("text/html;charset=UTF-8");
		PrintWriter out = resp.getWriter();
		
        if(list.size()>=10){//单词数大于或等于10的时候，输出频数前10个
            for(int i = 0; i<10; i++){
                out.println(list.get(i).getKey()+ ": " +list.get(i).getValue());
                out.println("<br>");
            }
        }else {
            for(int i = 0; i<list.size(); i++){//单词数小于10的时候，输出全部
            	out.println(list.get(i).getKey()+ ": " +list.get(i).getValue());
            	out.println("<br>");
            }
        }

    }
    
    
    	//前k多的单词
    public static void sortMap1(Map<String,Integer> oldmap,int k,HttpServletRequest req, HttpServletResponse resp) throws IOException{
        ArrayList<Map.Entry<String,Integer>> list = new ArrayList<Map.Entry<String,Integer>>(oldmap.entrySet());
        Collections.sort(list,new Comparator<Map.Entry<String,Integer>>(){
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                if (o1.getValue() == o2.getValue()) {
                    return o1.getKey().compareTo(o2.getKey());//词频相同时，按字典排列
                }
                return o2.getValue() - o1.getValue();  //降序排列
            }
        });
        resp.setContentType("text/html;charset=UTF-8");
		PrintWriter out = resp.getWriter();
        if(list.size()>=k){//单词数大于或等于k的时候，输出频数前k个
            for(int i = 0; i<k; i++){
                out.println(list.get(i).getKey()+ ": " +list.get(i).getValue());
            }
        }else {
            for(int i = 0; i<list.size(); i++){//单词数小于k的时候，输出全部
                out.println(list.get(i).getKey()+ ": " +list.get(i).getValue());//键值对一一对应
            }
        }

    }

    	//单词的词频统计
    public static void wordCount( Map<String, Integer> wordsCount,List<String> list1,HttpServletRequest req, HttpServletResponse resp) throws IOException{
    	resp.setContentType("text/html;charset=UTF-8");
		PrintWriter out = resp.getWriter();
        int countWord=0;
        
        for (String i : list1) {
            if(wordsCount.get(i) != null){
                wordsCount.put(i,wordsCount.get(i) + 1);
            }else{
                wordsCount.put(i,1);
            }
            countWord++;
            }
            out.println("单词数:"+countWord);
    }
    	
    	//输出到文件中
    public static void Output(int charCount,int lineCount,List<String> list,HttpServletRequest req, HttpServletResponse resp){
    	try {
            /* 写入Txt文件 */
            File writename = new File("d:/doutput.txt"); //如果没有则要建立一个新的output.txt文件
            writename.createNewFile(); // 创建新文件
            BufferedWriter out = new BufferedWriter(new FileWriter(writename));
            
            PrintStream printStream = new PrintStream(new FileOutputStream(writename));
            System.setOut(printStream);
            System.out.println("字符数:"+charCount);
            System.out.println("有效行数:"+lineCount);
            System.out.println("words:"+list.size());
            Map<String, Integer> wordsCount1 = new TreeMap<String,Integer>();  //存储单词计数信息，key值为单词，value为单词数
            wordCount(wordsCount1,list,req,resp);
            sortMap(wordsCount1,req,resp);
            out.flush(); // 把缓存区内容压入文件
            out.close(); 
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    
    //统计指定长度的词组的词频
    public static void lenWordCount(Map<String,Integer> oldmap,int n,HttpServletRequest req, HttpServletResponse resp) throws IOException {
    	ArrayList<Map.Entry<String,Integer>> list = new ArrayList<Map.Entry<String,Integer>>(oldmap.entrySet());
        Collections.sort(list,new Comparator<Map.Entry<String,Integer>>(){
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                if (o1.getValue() == o2.getValue()) {
                    return o1.getKey().compareTo(o2.getKey());//词频相同时，按字典排列
                }
                return o2.getValue() - o1.getValue();  //降序排列
            }
        });
        resp.setContentType("text/html;charset=UTF-8");
		PrintWriter out = resp.getWriter();
		
        for(int i = 0;i < list.size();i++) {
        	if(list.get(i).getKey().length()==n) {
        		out.println(list.get(i).getKey()+ ": " +list.get(i).getValue());
        	}
        }
    }
	
	
	 protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			   throws ServletException, IOException {
		   
		    resp.setContentType("text/html;charset=UTF-8");
			PrintWriter out = resp.getWriter();
		    //读入TXT文件
	    	Scanner input = new Scanner(System.in);
	    	String pathname = req.getParameter("txt");
	    	
	        int charCount=0;
	        int lineCount=0;
	        InputStreamReader r = new InputStreamReader(new FileInputStream(pathname)); // 建立一个输入流对象reader
	        BufferedReader br = new BufferedReader(r); // 建立一个对象，它把文件内容转成计算机能读懂的语言
	        
	        List<String> list = new ArrayList<String>();  //存储单词
	        String readLine = null;
	        while((readLine = br.readLine()) != null){
	            charCount += readLine.length();//字符个数就是字符长度，不考虑汉字
	            if(readLine.isEmpty()) {
	            	//此行为空，为无效行
	            }
	            else {
	            	lineCount++;}//一行一行读，每读一行行数加1
	            String[] wordsArrays = readLine.split("\\s*[^0-9a-zA-Z]+");
	            //以空格和非字母数字符号分割，至少4个英文字母开头，跟上字母数字符号
	            for (String word : wordsArrays) {
	                if(word.matches("[a-zA-Z]{4,}[a-zA-Z0-9]*")){
	                    list.add(word.toLowerCase());
	                }
	            }
	        }
	        
	        r.close();//关闭文件
	        
	        Map<String, Integer> wordsCount = new TreeMap<String,Integer>();  //存储单词计数信息，key值为单词，value为单词数
	       
	        
	        out.println("<html>");
	        out.println("<head>");
	        out.println("<title>输出</title>");
	        out.println("</head>");
	        out.println("<style>");
	        out.println("h1{text-shadow: 5px 5px 5px #FF0000;}");
	        out.println("</style>");
	        out.println("<body background=\"背景.jpg\" style=\"background-repeat: no-repeat; background-size: 100% 100%; background-attachment: fixed;\">");
	        out.println("<center>");
	        out.println("<h1>欢迎您使用宁&瑀的小程序!</h1>");
	        out.println("<h3>字符统计结果</h3>");
	        out.println("字符数:"+charCount);
	        out.println("<br>");
	        out.println("有效行数:"+lineCount);
	        out.println("<br>");
	  
	        wordCount(wordsCount,list,req,resp);
	        out.println("<br>");
	        sortMap(wordsCount,req,resp);    //按值进行排序
	       
	        
	        out.println("<input type=\"button\" value=\"查看前k个单词\" onclick=\"window.location.href='other.jsp'\"> ");
	        out.println("<input type=\"button\" value=\"查看指定长度单词\" onclick=\"window.location.href='abc.jsp'\"> ");
	        out.println("<input type=\"button\" value=\"返回\" onclick=\"window.location.href='index.jsp'\"> ");
	        
	        out.println("</center>");
	        out.println("</body>");
	        out.println("</html>");

			 }
	 
	 protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost( req,resp);
	 }
	 

}
